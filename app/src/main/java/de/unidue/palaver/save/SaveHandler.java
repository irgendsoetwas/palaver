package de.unidue.palaver.save;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import de.unidue.palaver.chat.Message;

public class SaveHandler extends SQLiteOpenHelper {

    private static final String TAG = "SQLite";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "MessageSaver";

    // Table name: Note.
    private static final String TABLE_MESSAGE = "Message";

    private static final String COLUMN_ID = "ID";
    private static final String COLUMN_SENDER = "Sender";
    private static final String COLUMN_RECIPIENT = "Empfaenger";
    private static final String COLUMN_MIMETYPE = "Mimetype";
    private static final String COLUMN_DATA = "Text";
    private static final String COLUMN_DATETIME = "Datum";

    public SaveHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Create table
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(TAG, "MyDatabaseHelper.onCreate ... ");
        // Script.

        String script = "CREATE TABLE " + TABLE_MESSAGE + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY,"
                + COLUMN_SENDER + " TEXT,"
                + COLUMN_RECIPIENT + " TEXT,"
                + COLUMN_MIMETYPE + " Text,"
                + COLUMN_DATA + " TEXT,"
                + COLUMN_DATETIME + " Text"
                + ")";
        // Execute Script.
        db.execSQL(script);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.i(TAG, "MyDatabaseHelper.onUpgrade ... ");
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGE);

        // Create tables again
        onCreate(db);
    }

    public void addAllMessages(ArrayList<Message> messages) {
        for (Message message : messages) {
            addMessage(message);
        }

    }

    public void addMessage(Message msg) {
        Log.i(TAG, "MyDatabaseHelper.addNote ... " + msg.getSender() + msg.getRecipient() + msg.getData());

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_SENDER, msg.getSender());
        values.put(COLUMN_RECIPIENT, msg.getRecipient());
        values.put(COLUMN_MIMETYPE, msg.getMimetype());
        values.put(COLUMN_DATA, msg.getData());
        values.put(COLUMN_DATETIME, msg.getDatetime());

        // Inserting Row
        db.insert(TABLE_MESSAGE, null, values);

        // Closing database connection
        db.close();
    }

    /*
        public Message getNote(int id) {
            Log.i(TAG, "MyDatabaseHelper.getNote ... " + id);

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.query(TABLE_MESSAGE, new String[] {COLUMN_SENDER,
                            COLUMN_RECIPIENT, COLUMN_DATA}, COLUMN_SENDER + "=?",
                    new String[] { String.valueOf(id) }, null, null, null, null);
            if (cursor != null)
                cursor.moveToFirst();

            Message note = new Message(Integer.parseInt(
                   cursor.getString(0)),
                  cursor.getString(1),
                   cursor.getString(2));
             return note
            return note;
        }

    */
    public ArrayList<String> getAllChats() {
        ArrayList<String> chatpartner = new ArrayList<String>();
        // Select All Query
        String selectQuery = "SELECT DISTINCT " + COLUMN_RECIPIENT + " FROM " + TABLE_MESSAGE;


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                chatpartner.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        return chatpartner;
    }

    public ArrayList<Message> getAllMessages(String recip) {
        Log.i(TAG, "MyDatabaseHelper.getAllNotes ... ");

        ArrayList<Message> msgs = new ArrayList<Message>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_MESSAGE + " WHERE (" + COLUMN_SENDER + "=" + recip + ") OR (" + COLUMN_RECIPIENT + "=" + recip + ")";

        Log.d("db", selectQuery);

        SQLiteDatabase db = this.getWritableDatabase();
        //Cursor cursor = db.rawQuery(selectQuery, null);

        Cursor cursor = db.query(TABLE_MESSAGE, new String[]{COLUMN_SENDER, COLUMN_RECIPIENT, COLUMN_MIMETYPE, COLUMN_DATA, COLUMN_DATETIME}, COLUMN_SENDER + "=? OR " + COLUMN_RECIPIENT + "=?", new String[]{recip, recip}, null, null, null, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Message msg = new Message(
                        (cursor.getString(0)),
                        (cursor.getString(1)),
                        (cursor.getString(2)),
                        (cursor.getString(3)),
                        (cursor.getString(4)));

/*
                msg.setNoteId(Integer.parseInt(cursor.getString(0)));
                note.setNoteTitle(cursor.getString(1));
                note.setNoteContent(cursor.getString(2));
 */

                //Adding note to list
                msgs.add(msg);
            } while (cursor.moveToNext());
        }

        // return note list
        return msgs;
    }


    /*public int getNotesCount() {
        Log.i(TAG, "MyDatabaseHelper.getNotesCount ... " );

        String countQuery = "SELECT  * FROM " + TABLE_MESSAGE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();

        cursor.close();

        // return count
        return count;
    }


    public int updateNote(Note note) {
        Log.i(TAG, "MyDatabaseHelper.updateNote ... "  + note.getNoteTitle());

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_RECIPIENT, note.getNoteTitle());
        values.put(COLUMN_DATA, note.getNoteContent());

        // updating row
        return db.update(TABLE_MESSAGE, values, COLUMN_SENDER + " = ?",
                new String[]{String.valueOf(note.getNoteId())});
    }

    public void deleteNote(Note note) {
        Log.i(TAG, "MyDatabaseHelper.updateNote ... " + note.getNoteTitle() );

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_MESSAGE, COLUMN_SENDER + " = ?",
                new String[] { String.valueOf(note.getNoteId()) });
        db.close();
    }
*/
}