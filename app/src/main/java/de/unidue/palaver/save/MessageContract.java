package de.unidue.palaver.save;

import android.provider.BaseColumns;

public final class MessageContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private MessageContract() {}

    /* Inner class that defines the table contents */
    public static class FeedEntry implements BaseColumns {
        private static final String COLUMN_SENDER ="Sender";
        private static final String COLUMN_RECIPIENT ="Empfaenger";
        private static final String COLUMN_MIMETYPE = "Mimetype";
        private static final String COLUMN_DATA = "Text";
        private static final String COLUMN_DATETIME = "Datum";
    }
}
