package de.unidue.palaver.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import de.unidue.palaver.MainActivity;
import de.unidue.palaver.R;

public class LoginActivity extends AppCompatActivity {

    private LoginHandler loginHandler;
    private RegisterHandler registerHandler;
    private EditText usernameEditText;
    private EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginHandler = new LoginHandler(this);
        registerHandler = new RegisterHandler(this);

        usernameEditText = findViewById(R.id.username);
        passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.btn_login);
        final Button registerButton = findViewById(R.id.btn_register);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress();
                loginHandler.login(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString()
                );
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(usernameEditText.getText())) {
                    usernameEditText.setError("Benutzer darf nicht leer sein");
                }
                else if (TextUtils.isEmpty(passwordEditText.getText())) {
                    passwordEditText.setError("Passwort darf nicht leer sein");
                }
                else {
                    registerHandler.register(usernameEditText.getText().toString(),
                            passwordEditText.getText().toString()
                    );
                }
            }
        });
    }

    public void changeView() {
        hideProgress();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void showError(String info) {
        hideProgress();
        if (info.contains("Benutzer")) {
            usernameEditText.setError(info);
        }
        if (info.contains("Passwort")) {
            passwordEditText.setError(info);
        }
    }

    /** Show progress spinner and disable buttons **/
    private void showProgress() {
        findViewById(R.id.progressBar).setVisibility(View.VISIBLE);

        // Disable all buttons while progress indicator shows.
        setViewsEnabled(false, R.id.btn_login, R.id.btn_register);
    }

    /** Hide progress spinner and enable buttons **/
    private void hideProgress() {
        findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);

        // Enable buttons once progress indicator is hidden.
        setViewsEnabled(true, R.id.btn_login, R.id.btn_register);
    }

    /** Enable or disable multiple views **/
    private void setViewsEnabled(boolean enabled, int... ids) {
        for (int id : ids) {
            findViewById(id).setEnabled(enabled);
        }
    }
}
