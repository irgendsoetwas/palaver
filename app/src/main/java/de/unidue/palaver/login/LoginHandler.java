package de.unidue.palaver.login;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import de.unidue.palaver.data.SecurePreferences;
import de.unidue.palaver.network.CustomCallback;
import de.unidue.palaver.network.HttpAsyncTask;
import de.unidue.palaver.network.RequestType;
import de.unidue.palaver.network.ServerResponse;

class LoginHandler implements CustomCallback {

    private LoginActivity loginActivity;
    private Context context;

    private String username;
    private String password;

    LoginHandler(LoginActivity loginActivity) {
        this.loginActivity = loginActivity;
        this.context = loginActivity.getApplicationContext();
    }

    void login(String username, String password) {
        try {
            this.username = username;
            this.password = password;

            JSONObject jsonParam = new JSONObject();
            jsonParam.put("Username", username);
            jsonParam.put("Password", password);

            HttpAsyncTask task = new HttpAsyncTask(jsonParam, RequestType.LOGIN, this);
            task.execute("http://palaver.se.paluno.uni-due.de/api/user/validate");
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void completionHandler(Boolean success, RequestType type, final ServerResponse response) {
        if (type == RequestType.LOGIN) {
            if (success) {
                if (response.isSuccess()) {
                    Log.d("login", "success!");

                    saveCredentials();

                    loginActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loginActivity.changeView();
                        }
                    });


                }
                else {
                    Log.d("login", response.getInfo()); // entweder Benutzer existiert nicht oder Passwort falsch

                    loginActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loginActivity.showError(response.getInfo());
                        }
                    });
                }
            }
            else {
                Log.d("login", "failed getting data");
                // TODO handle fatal error when getting data
            }
        }
    }

    private void saveCredentials() {
        final SharedPreferences sp = new SecurePreferences(context, context.getSharedPreferences("loginPalaver", Context.MODE_PRIVATE) );
        sp.edit().putString("Username", username).apply();
        sp.edit().putString("Password", password).apply();
        Log.d("login", "setting creds success");

        // successfully logging in means create notification id
        String LAST_NOTIF_ID = "LAST_NOTIF_ID";
        final SharedPreferences spNotificationID = new SecurePreferences(context, context.getSharedPreferences("LAST_NOTIF_ID", Context.MODE_PRIVATE) );
        spNotificationID.edit().putInt(LAST_NOTIF_ID, 0).apply();
    }
}
