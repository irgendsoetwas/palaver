package de.unidue.palaver.login;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import de.unidue.palaver.MainActivity;
import de.unidue.palaver.R;
import de.unidue.palaver.data.SecurePreferences;
import de.unidue.palaver.network.CustomCallback;
import de.unidue.palaver.network.HttpAsyncTask;
import de.unidue.palaver.network.RequestType;
import de.unidue.palaver.network.ServerResponse;

public class ChangePasswordHandler implements CustomCallback {
    private MainActivity mainActivity;
    private Context context;
    private String username;
    private String newPassword;

    public ChangePasswordHandler(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.context = mainActivity.getApplicationContext();
    }

    public void changePassword(String newPassword) {
        try {
            final SharedPreferences sp = new SecurePreferences(context, context.getSharedPreferences("loginPalaver", Context.MODE_PRIVATE));
            this.username = sp.getString("Username", "");
            String password = sp.getString("Password", "");
            this.newPassword = newPassword;

            JSONObject jsonParam = new JSONObject();
            jsonParam.put("Username", username);
            jsonParam.put("Password", password);
            jsonParam.put("NewPassword", newPassword);

            HttpAsyncTask task = new HttpAsyncTask(jsonParam, RequestType.PASSWORD_CHANGE, this);
            task.execute("http://palaver.se.paluno.uni-due.de/api/user/password");
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void completionHandler(Boolean success, RequestType type, final ServerResponse responseObject) {
        if (type == RequestType.PASSWORD_CHANGE) {
            if (success) {
                if (responseObject.isSuccess()) {
                    Log.d("password", "success! " + responseObject.getInfo());
                    saveCredentials();

                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Snackbar.make(Objects.requireNonNull(mainActivity.findViewById(R.id.root)), responseObject.getInfo(), Snackbar.LENGTH_SHORT).show();

                        }
                    });
                }
                else {
                    Log.d("password", responseObject.getInfo()); // entweder Benutzer existiert nicht oder Passwort falsch

                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Snackbar.make(Objects.requireNonNull(mainActivity.findViewById(R.id.root)), responseObject.getInfo(), Snackbar.LENGTH_SHORT).show();
                        }
                    });
                }
            }
            else {
                Log.d("login", "failed getting data");
                // TODO handle fatal error when getting data
            }
        }
    }

    private void saveCredentials() {
        final SharedPreferences sp = new SecurePreferences(context, context.getSharedPreferences("loginPalaver", Context.MODE_PRIVATE) );
        sp.edit().putString("Username", username).apply();
        sp.edit().putString("Password", newPassword).apply();
        Log.d("login", "setting creds success");
    }
}
