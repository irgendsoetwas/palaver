package de.unidue.palaver.login;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import de.unidue.palaver.network.CustomCallback;
import de.unidue.palaver.network.HttpAsyncTask;
import de.unidue.palaver.network.RequestType;
import de.unidue.palaver.network.ServerResponse;

public class RegisterHandler implements CustomCallback {
    private LoginActivity loginActivity;

    public RegisterHandler(LoginActivity loginActivity) {
        this.loginActivity = loginActivity;
    }

    void register(String username, String password) {
        try {
            JSONObject jsonParam = new JSONObject();
            jsonParam.put("Username", username);
            jsonParam.put("Password", password);

            HttpAsyncTask task = new HttpAsyncTask(jsonParam, RequestType.REGISTER, this);
            task.execute("http://palaver.se.paluno.uni-due.de/api/user/register");
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void completionHandler(Boolean success, RequestType type, final ServerResponse response) {
        if (type == RequestType.REGISTER) {
            if (success) {
                if (response.isSuccess()) {
                    Log.d("register", "success!");
                    loginActivity.changeView();
                }
                else {
                    Log.d("register", response.getInfo()); //

                    loginActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loginActivity.showError(response.getInfo());
                        }
                    });
                }
            }
            else {
                Log.d("register", "failed getting data");
                // TODO handle fatal error when getting data
            }
        }
    }
}
