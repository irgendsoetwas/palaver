package de.unidue.palaver;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.Objects;

import de.unidue.palaver.chat.ChatlistFragment;
import de.unidue.palaver.data.SecurePreferences;
import de.unidue.palaver.friendlist.FriendlistFragment;
import de.unidue.palaver.login.ChangePasswordHandler;
import de.unidue.palaver.login.LoginActivity;
import de.unidue.palaver.network.PushTokenHandler;
import de.unidue.palaver.save.SaveHandler;
import de.unidue.palaver.settings.SettingsFragment;


public class MainActivity extends AppCompatActivity implements SettingsFragment.OnOptionClickListener {

    public static boolean online = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestUserData();

        final ViewPager viewPager = findViewById(R.id.pager);
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        final TabPagerAdapter adapter = new TabPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FriendlistFragment(), "Freunde");
        adapter.addFragment(new ChatlistFragment(), "Chats");
        adapter.addFragment(new SettingsFragment(), "Einstellungen");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(1);
    }

    public void requestUserData() {
        final SharedPreferences sp = new SecurePreferences(this, this.getSharedPreferences("loginPalaver", Context.MODE_PRIVATE));
        String username = sp.getString("Username", "");
        String password = sp.getString("Password", "");

        if (hasNetwork()) {//online
                Log.d("login","online");
            if (username.isEmpty() && password.isEmpty()) {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                Log.d("login", "getting creds failed");
            } else if (!username.isEmpty() && !password.isEmpty()) {
                retrieveToken(false);
                Log.d("login", "getting creds success");
                //online und eingeloggt
                online = true;

            }
        }else{
            Log.d("login","offline");
            if (username.isEmpty() && password.isEmpty()) { //offline
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                Log.d("login", "getting creds failed");
            } else if (!username.isEmpty() && !password.isEmpty()) {
                retrieveToken(false);
                //load sqlite chats
                Log.d("login", "getting creds success");
                //offline und eingeloggt
                online =false;

            }
        }
    }

    private boolean hasNetwork()
    {
        try
        {
            ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo().isConnectedOrConnecting();
        }
        catch (Exception e)
        {
            return false;
        }
    }



    private void logoutUser() {
        final SharedPreferences sp = new SecurePreferences(this, this.getSharedPreferences("loginPalaver", Context.MODE_PRIVATE) );

        // empty token for server
        registerToken("");

        // clear SharedPreferences
        sp.edit().putString("Username", "").apply();
        sp.edit().putString("Password", "").apply();
        Log.d("login", "deleted creds success");
    }

    @Override
    public void onOptionSelected(String option) {
        switch (option) {
            case "logout":
                logoutUser();
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;
            case "info":
                retrieveToken(true);
                break;
            case "passwordChange":
                changePassword();
                break;
            default:
                break;
        }
    }

    private void retrieveToken(final boolean informative) {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.d("Firebase", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = Objects.requireNonNull(task.getResult()).getToken();

                        if (informative) {
                            // Log and toast
                            Log.d("Firebase", "Refreshed token: " + token);
                            showPopup(token);
                        }
                        else {
                            registerToken(token);
                        }
                    }
                });
    }

    public void registerToken(String token) {
        PushTokenHandler pth = new PushTokenHandler(this);
        pth.transmitToken(token);
    }

    private void showPopup(String token) {
        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_window, null);

        final SharedPreferences sp = new SecurePreferences(this, this.getSharedPreferences("loginPalaver", Context.MODE_PRIVATE) );
        String username = sp.getString("Username", "");
        TextView text = popupView.findViewById(R.id.popupText);
        text.setText("User: " + username + "\n\nToken: " + token);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, true);


        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window
        popupWindow.showAtLocation(findViewById(R.id.root), Gravity.CENTER, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    private void changePassword() {
        final EditText edtText = new EditText(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Passwort ändern");
        builder.setMessage("Neues Passwort:");
        builder.setCancelable(false);
        builder.setView(edtText);
        builder.setPositiveButton(getString(R.string.label_confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String input = edtText.getText().toString();
                if (input.isEmpty()) {
                    Snackbar.make(findViewById(R.id.root), "Passwort leer", Snackbar.LENGTH_SHORT).show();
                    return;
                }
                ChangePasswordHandler cph = new ChangePasswordHandler(MainActivity.this);
                cph.changePassword(input);
            }
        });
        builder.setNegativeButton(getString(R.string.label_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Snackbar.make(findViewById(R.id.root), R.string.action_cancel, Snackbar.LENGTH_SHORT).show();
            }
        });
        builder.show();
    }

    public void setFriendlistTab() {
        final ViewPager viewPager = findViewById(R.id.pager);
        viewPager.setCurrentItem(0);
    }
}
