package de.unidue.palaver.friendlist;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import de.unidue.palaver.chat.ChatlistFragment;
import de.unidue.palaver.data.SecurePreferences;
import de.unidue.palaver.network.CustomCallback;
import de.unidue.palaver.network.HttpAsyncTask;
import de.unidue.palaver.network.RequestType;
import de.unidue.palaver.network.ServerResponse;

public class FriendlistHandler implements CustomCallback {

    private Context context;
    private Fragment fragment;

    public FriendlistHandler(Fragment ff, Context context) {
        this.context = context;
        this.fragment = ff;
    }

    public void getFriendlist() {
        try {
            final SharedPreferences sp = new SecurePreferences(context, context.getSharedPreferences("loginPalaver", Context.MODE_PRIVATE));
            String username = sp.getString("Username", "");
            String password = sp.getString("Password", "");

            JSONObject jsonParam = new JSONObject();
            jsonParam.put("Username", username);
            jsonParam.put("Password", password);

            HttpAsyncTask task = new HttpAsyncTask(jsonParam, RequestType.FRIENDLIST, this);
            task.execute("http://palaver.se.paluno.uni-due.de/api/friends/get");
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void completionHandler(Boolean success, RequestType type, ServerResponse responseObject) {
        if (type == RequestType.FRIENDLIST) {
            if (success) {
                if (responseObject.isSuccess()) {
                    Log.d("FriendlistHandler", "success! " + responseObject.getInfo());

                    String friends = responseObject.getData();

                    if (fragment instanceof FriendlistFragment){
                        ((FriendlistFragment) fragment).setFriendslist(friends);
                    }else if (fragment instanceof ChatlistFragment){
                        ((ChatlistFragment) fragment).setFriendslist(friends);
                    }
                }
                else {
                    Log.d("FriendlistHandler", responseObject.getInfo());
                }
            }
            else {
                Log.d("login", "failed getting data");
                // TODO handle fatal error when getting data
            }
        }
    }
}
