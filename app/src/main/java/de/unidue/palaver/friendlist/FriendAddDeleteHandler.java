package de.unidue.palaver.friendlist;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import de.unidue.palaver.data.SecurePreferences;
import de.unidue.palaver.network.CustomCallback;
import de.unidue.palaver.network.HttpAsyncTask;
import de.unidue.palaver.network.RequestType;
import de.unidue.palaver.network.ServerResponse;

public class FriendAddDeleteHandler implements CustomCallback {
    private Context context;
    private FriendlistFragment friendlistFragment;

    FriendAddDeleteHandler(FriendlistFragment ff, Context context) {
        this.context = context;
        this.friendlistFragment = ff;
    }

    private JSONObject prepareCall(String newFriend) {
        JSONObject jsonParam = new JSONObject();
        try {
            final SharedPreferences sp = new SecurePreferences(context, context.getSharedPreferences("loginPalaver", Context.MODE_PRIVATE));
            String username = sp.getString("Username", "");
            String password = sp.getString("Password", "");

            jsonParam.put("Username", username);
            jsonParam.put("Password", password);
            jsonParam.put("Friend", newFriend);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return jsonParam;
    }

    void addFriend(String newFriend) {
        JSONObject jsonParam = prepareCall(newFriend);

        HttpAsyncTask task = new HttpAsyncTask(jsonParam, RequestType.FRIENDLIST, this);
        task.execute("http://palaver.se.paluno.uni-due.de/api/friends/add");
    }

    void deleteFriend(String newFriend) {
        JSONObject jsonParam = prepareCall(newFriend);

        HttpAsyncTask task = new HttpAsyncTask(jsonParam, RequestType.FRIENDLIST, this);
        task.execute("http://palaver.se.paluno.uni-due.de/api/friends/remove");
    }

    @Override
    public void completionHandler(Boolean success, RequestType type, ServerResponse responseObject) {
        if (type == RequestType.FRIENDLIST) {
            if (success) {
                if (responseObject.isSuccess()) {
                    Log.d("FriendAddDeleteHandler", "success! " + responseObject.getInfo());
                    Snackbar.make(Objects.requireNonNull(friendlistFragment.getView()), responseObject.getInfo(), Snackbar.LENGTH_SHORT).show();
                    friendlistFragment.flh.getFriendlist();
                }
                else {
                    Log.d("FriendAddDeleteHandler", responseObject.getInfo());
                    Snackbar.make(Objects.requireNonNull(friendlistFragment.getView()), responseObject.getInfo(), Snackbar.LENGTH_SHORT).show();
                }
            }
            else {
                Log.d("login", "failed getting data");
                // TODO handle fatal error when getting data
            }
        }
    }
}
