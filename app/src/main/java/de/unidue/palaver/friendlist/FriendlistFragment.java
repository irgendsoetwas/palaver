package de.unidue.palaver.friendlist;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Objects;

import de.unidue.palaver.MainActivity;
import de.unidue.palaver.R;
import de.unidue.palaver.SwipeController;
import de.unidue.palaver.SwipeControllerActions;

/**
 * A simple {@link Fragment} subclass.
 */
public class FriendlistFragment extends Fragment {

    private FriendlistRecyclerAdapter mAdapter;
    private RecyclerView recyclerView;
    FriendlistHandler flh;

    public FriendlistFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_friends, container, false);

        // get ui elements
        recyclerView = rootView.findViewById(R.id.recyclerView);
        final FloatingActionButton fab = rootView.findViewById(R.id.fab);

        // to hide the fab while scrolling downwards
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && fab.getVisibility() == View.VISIBLE) {
                    fab.hide();
                } else if (dy < 0 && fab.getVisibility() != View.VISIBLE) {
                    fab.show();
                }
            }
        });

        // floating action button for adding friends
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText edtText = new EditText(getActivity());

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Freund hinzufügen");
                builder.setMessage("Name des Freundes:");
                builder.setCancelable(false);
                builder.setView(edtText);
                builder.setPositiveButton(getString(R.string.label_confirm), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String input = edtText.getText().toString();
                        if (input.isEmpty()) {
                            Snackbar.make(Objects.requireNonNull(getView()), R.string.friend_empty, Snackbar.LENGTH_SHORT).show();
                            return;
                        }

                        FriendAddDeleteHandler fadh = new FriendAddDeleteHandler(FriendlistFragment.this, getActivity());
                        fadh.addFriend(input);
                    }
                });
                builder.setNegativeButton(getString(R.string.label_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Snackbar.make(Objects.requireNonNull(getView()), R.string.action_cancel, Snackbar.LENGTH_SHORT).show();
                    }
                });
                builder.show();
            }
        });

        // add swiping controller for button
        final SwipeController swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                String friendToDelete = mAdapter.getFriend(position);
                FriendAddDeleteHandler fadh = new FriendAddDeleteHandler(FriendlistFragment.this, getActivity());
                fadh.deleteFriend(friendToDelete);
            }
        }, getContext());
        //swiping logic
        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // 2. set layoutManger
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        flh = new FriendlistHandler(this, getActivity());
        flh.getFriendlist();

        // 3. create an adapter
        mAdapter = new FriendlistRecyclerAdapter((MainActivity)getActivity());
        // 4. set adapter
        recyclerView.setAdapter(mAdapter);
        // 5. set item animator to DefaultAnimator
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return rootView;
    }

    public void setFriendslist(String friends) {
        try {
            String[] friendArray = jsonStringToArray(friends);

            mAdapter.setDataset(friendArray);

            recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

   /* public  String[] getFriendArray(){
        try{
            String[] friendArray = jsonStringToArray(friends);
            return friendArray;
        }catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("getFriendArray","keine freund array returned");
        return null;

    }*/

    private String[] jsonStringToArray(String jsonString) throws JSONException {
        ArrayList<String> stringArray = new ArrayList<>();
        JSONArray jsonArray = new JSONArray(jsonString);

        for (int i = 0; i < jsonArray.length(); i++) {
            stringArray.add(jsonArray.getString(i));
        }

        String[] stockArr = new String[stringArray.size()];
        stockArr = stringArray.toArray(stockArr);

        return stockArr;
    }

}
