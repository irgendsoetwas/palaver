package de.unidue.palaver.chat;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.unidue.palaver.MainActivity;
import de.unidue.palaver.R;

public class ChatlistRecyclerAdapter extends RecyclerView.Adapter<ChatlistRecyclerAdapter.ViewHolder> {
    private String[] friendsData = {""}; // emtpy string array for avoiding null pointer
    private MainActivity m;

    // inner class to hold a reference to each item of RecyclerView
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtViewTitle;
        TextView initialsViewItem;

        ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txtViewTitle = itemLayoutView.findViewById(R.id.item_title);
            initialsViewItem = itemLayoutView.findViewById(R.id.item_initials);
        }
    }

    ChatlistRecyclerAdapter(MainActivity m) {
        this.m = m;
    }

    // Create new views (invoked by the layout manager)
    //fuer fragment
    @NonNull
    @Override
    public ChatlistRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_layout, parent, false);

        // create ViewHolder
        return new ChatlistRecyclerAdapter.ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull ChatlistRecyclerAdapter.ViewHolder viewHolder, int position) {
        // - get data from your friendsData at this position
        // - replace the contents of the view with that friendsData

        final String currentFriend = friendsData[position];

        // if name is empty, dont even try to do something
        if (currentFriend.length() < 1) {
            return;
        }

        viewHolder.txtViewTitle.setText(currentFriend);
        viewHolder.initialsViewItem.setText(currentFriend.trim().substring(0,1).toUpperCase());

        viewHolder.txtViewTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("friend", currentFriend);
                final Intent intent = new Intent(m, ChatActivity.class);
                intent.putExtra("recipient",currentFriend);
                m.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        m.startActivity(intent);
                    }
                });
            }
        });

        viewHolder.initialsViewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("friend", currentFriend);
                final Intent intent = new Intent(m, ChatActivity.class);
                intent.putExtra("recipient",currentFriend);
                m.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        m.startActivity(intent);
                    }
                });
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return friendsData.length;
    }

    void setDataset(String[] data) {
        friendsData = data;
    }

    String getFriend(int position) {
        return friendsData[position];
    }
}