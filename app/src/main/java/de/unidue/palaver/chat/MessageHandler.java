package de.unidue.palaver.chat;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.unidue.palaver.data.SecurePreferences;
import de.unidue.palaver.network.CustomCallback;
import de.unidue.palaver.network.HttpAsyncTask;
import de.unidue.palaver.network.RequestType;
import de.unidue.palaver.network.ServerResponse;
import de.unidue.palaver.save.SaveHandler;

public class MessageHandler implements CustomCallback {

    private Context context;
    private ChatActivity chatActivity;
    private String recip;
    private ArrayList<Message> messages = new ArrayList<Message>();
    private String mimetype;
    private String recipp;

    public MessageHandler(ChatActivity ca, Context context) {
        this.context = context;
        this.chatActivity = ca;
    }

    public void getMessage(String recip) {
        try {
            final SharedPreferences sp = new SecurePreferences(context, context.getSharedPreferences("loginPalaver", Context.MODE_PRIVATE));
            String username = sp.getString("Username", "");
            String password = sp.getString("Password", "");
            this.recip = recip;

            JSONObject jsonParam = new JSONObject();
            jsonParam.put("Username", username);
            jsonParam.put("Password", password);
            jsonParam.put("Recipient", recip);

            HttpAsyncTask task = new HttpAsyncTask(jsonParam, RequestType.GET_MESSAGE, this);
            task.execute("http://palaver.se.paluno.uni-due.de/api/message/get");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String recipp, String data, String mimetype) {
        try {
            final SharedPreferences sp = new SecurePreferences(context, context.getSharedPreferences("loginPalaver", Context.MODE_PRIVATE));
            String username = sp.getString("Username", "");
            String password = sp.getString("Password", "");
            this.recipp = recipp;
            this.mimetype = mimetype;
            String data1 = data;

            JSONObject json = new JSONObject();
            json.put("Username", username);
            json.put("Password", password);
            json.put("Recipient", recipp);
            json.put("Mimetype", mimetype);
            json.put("Data", data);

            HttpAsyncTask task = new HttpAsyncTask(json, RequestType.SEND_MESSAGE, this);
            task.execute("http://palaver.se.paluno.uni-due.de/api/message/send");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void completionHandler(Boolean success, RequestType type, ServerResponse responseObject) {
        if (type == RequestType.GET_MESSAGE) {
            if (success) {
                if (responseObject.isSuccess()) {

                    String data = responseObject.getData();

                    if (!data.equals("[]")) {
                        try {
                            JSONArray jsonArray = new JSONArray(data);

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jo = jsonArray.getJSONObject(i);
                                Message message = new Message(
                                        jo.getString("Sender"),
                                        jo.getString("Recipient"),
                                        jo.getString("Mimetype"),
                                        jo.getString("Data"),
                                        jo.getString("DateTime")
                                );
                                SaveHandler db = new SaveHandler(context);
                                db.addMessage(message);
                                messages.add(message);

                            }

                            // TODO insert into db

                            chatActivity.setMessages(messages);

                        } catch (JSONException e) {
                            Log.d("MessageHandler", "Fehler im Handler bein JSON Parsen");
                            e.printStackTrace();
                        }
                        Log.d("MessageHandler", "Message not empty " + responseObject.getData() + recip);
                    }

                } else {
                    Log.d("FriendlistHandler", responseObject.getInfo());
                }
            } else {
                Log.d("login", "failed getting data");
                // TODO handle fatal error when getting data
            }
        }
        if (type == RequestType.SEND_MESSAGE) {
            if (success) {
                if (responseObject.isSuccess()) {
                    int msgtype = responseObject.getMsgType();
                    if (msgtype == 1) {
                        Log.d("Message Sender", "Message Success");

                        //todo Ding unten reinscheiuben mit erfolgreich gesandet
                        chatActivity.reloadMessages();

                    } else if (msgtype == 0) {
                        Log.d("Message Sender", "Message failed " + responseObject.getInfo());
                        //todo Fehlermeldung
                    } else {
                        Log.d("Message Sender", "Message failed Ganz komische serverantwort");
                    }
                }
            }
        }


    }

}
