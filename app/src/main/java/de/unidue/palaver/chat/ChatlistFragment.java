package de.unidue.palaver.chat;

import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Array;
import java.util.ArrayList;

import de.unidue.palaver.MainActivity;
import de.unidue.palaver.R;
import de.unidue.palaver.SwipeController;
import de.unidue.palaver.SwipeControllerActions;
import de.unidue.palaver.friendlist.FriendlistHandler;
import de.unidue.palaver.save.SaveHandler;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatlistFragment extends Fragment {


    private ChatlistRecyclerAdapter mAdapter;
    private RecyclerView recyclerView;
    FriendlistHandler flh;
    private ArrayList<String> chatFriends = new ArrayList<>();

    public ChatlistFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_chatlist, container, false);

        // get ui elements
        recyclerView = rootView.findViewById(R.id.recyclerView);
        final FloatingActionButton fab = rootView.findViewById(R.id.fab);

        // to hide the fab while scrolling downwards
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && fab.getVisibility() == View.VISIBLE) {
                    fab.hide();
                } else if (dy < 0 && fab.getVisibility() != View.VISIBLE) {
                    fab.show();
                }
            }
        });

        // floating action button for adding friends
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //todo switch to friendlist
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) getActivity()).setFriendlistTab();
                    }
                });
            }
        });
        // add swiping controller for button
        final SwipeController swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                String friendToDelete = mAdapter.getFriend(position);
            }
        }, getContext());

        //swiping logic
        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // 2. set layoutManger
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (MainActivity.online) {
            flh = new FriendlistHandler(this, getActivity());
            flh.getFriendlist();
        }else{
            SaveHandler db = new SaveHandler(getContext());
           chatFriends= db.getAllChats();
        }
        // 3. create an adapter
        mAdapter = new ChatlistRecyclerAdapter((MainActivity) getActivity());
        // 4. set adapter
        recyclerView.setAdapter(mAdapter);
        // 5. set item animator to DefaultAnimator
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if (!MainActivity.online){
            String[] temp = new String[chatFriends.size()];
            temp = chatFriends.toArray(temp);
            if (temp.length!=0) {
                mAdapter.setDataset(temp);
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }

        }

        return rootView;
    }

    public void setFriendslist(String friends) {
        try {
            String[] friendArray = jsonStringToArray(friends);

            for (String friend : friendArray) {
                ChatCheckHandler cch = new ChatCheckHandler(this, getActivity());
                cch.checkFriend(friend);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String[] jsonStringToArray(String jsonString) throws JSONException {
        ArrayList<String> stringArray = new ArrayList<>();
        JSONArray jsonArray = new JSONArray(jsonString);

        for (int i = 0; i < jsonArray.length(); i++) {
            stringArray.add(jsonArray.getString(i));
        }

        String[] stockArr = new String[stringArray.size()];
        stockArr = stringArray.toArray(stockArr);

        return stockArr;
    }

    public void addChatFriend(String chatter) {
        chatFriends.add(chatter);
        String[] temp = new String[chatFriends.size()];
        temp = chatFriends.toArray(temp);
        if (temp.length != 0) {
            mAdapter.setDataset(temp);

            recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.notifyDataSetChanged();
                }
            });
        } else Log.d("temp", "ist leer");
    }
}