package de.unidue.palaver.chat;

public class Message {
    private String sender;
    private String recipient;
    private String mimetype;
    private String data;
    private String datetime;

    public Message (String sender,
                    String recipient,
                    String mimetype,
                    String data,
                    String datetime){
        this.sender = sender;
        this.recipient=recipient;
        this.mimetype=mimetype;
        this.data=data;
        this.datetime=datetime;
    }

    public String getDatetime() {
        return datetime;
    }

    public String getData() {
        return data;
    }

    public String getMimetype() {
        return mimetype;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getSender() {
        return sender;
    }
}
