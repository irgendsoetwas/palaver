package de.unidue.palaver.chat;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import de.unidue.palaver.data.SecurePreferences;
import de.unidue.palaver.network.CustomCallback;
import de.unidue.palaver.network.HttpAsyncTask;
import de.unidue.palaver.network.RequestType;
import de.unidue.palaver.network.ServerResponse;

public class ChatCheckHandler implements CustomCallback {

    private Context context;
    private ChatlistFragment chatlistfragment;
    private String recip;

    public ChatCheckHandler(ChatlistFragment ff, Context context) {
        this.context = context;
        this.chatlistfragment = ff;
    }

    public void checkFriend(String recip) {
        try {
            final SharedPreferences sp = new SecurePreferences(context, context.getSharedPreferences("loginPalaver", Context.MODE_PRIVATE));
            String username = sp.getString("Username", "");
            String password = sp.getString("Password", "");
            this.recip = recip;

            JSONObject jsonParam = new JSONObject();
            jsonParam.put("Username", username);
            jsonParam.put("Password", password);
            jsonParam.put("Recipient", recip);

            HttpAsyncTask task = new HttpAsyncTask(jsonParam, RequestType.CHECK_FRIEND, this);
            task.execute("http://palaver.se.paluno.uni-due.de/api/message/get");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void completionHandler(Boolean success, RequestType type, ServerResponse responseObject) {
        if (type == RequestType.CHECK_FRIEND) {
            if (success) {
                if (responseObject.isSuccess()) {


                    String data = responseObject.getData();
                    if (!data.equals("[]")) {
                        chatlistfragment.addChatFriend(recip);
                    }

                } else {
                    Log.d("FriendlistHandler", responseObject.getInfo());
                }
            } else {
                Log.d("login", "failed getting data");
                // TODO handle fatal error when getting data
            }
        }
    }
}
