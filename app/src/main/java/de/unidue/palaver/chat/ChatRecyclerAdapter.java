package de.unidue.palaver.chat;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import de.unidue.palaver.R;
import de.unidue.palaver.data.SecurePreferences;

public class ChatRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
    private static final int VIEW_TYPE_LOCATION_SENT = 3;
    private static final int VIEW_TYPE_LOCATION_RECEIVED = 4;
    private static final int VIEW_TYPE_IMAGE_SENT = 5;
    private static final int VIEW_TYPE_IMAGE_RECEIVED = 6;

    private Context mContext;
    private ArrayList<Message> mMessageList = new ArrayList<>();

    // region Inner classes
    // inner class with binder for each type, to be called via onBindViewHolder
    class SentMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText;

        SentMessageHolder(View itemView) {
            super(itemView);

            messageText = itemView.findViewById(R.id.text_message_body);
            timeText = itemView.findViewById(R.id.text_message_time);
        }

        void bind(Message message) {
            messageText.setText(message.getData());

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.GERMANY);
            try {
                Date d = sdf.parse(message.getDatetime());

                // Format the stored timestamp into a readable String using method.
                String formattedDate = new SimpleDateFormat("dd.MM.yyyy - HH:mm", Locale.GERMANY).format(d);

                timeText.setText(formattedDate);
            } catch (ParseException ex) {
                Log.d("Dateparse", ex.getLocalizedMessage());
            }
        }
    }

    class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText;
        ImageView profileImage;

        ReceivedMessageHolder(View itemView) {
            super(itemView);

            messageText = itemView.findViewById(R.id.text_message_body);
            timeText = itemView.findViewById(R.id.text_message_time);
            profileImage = itemView.findViewById(R.id.image_message_profile);
        }

        void bind(Message message) {
            messageText.setText(message.getData());

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.GERMANY);
            try {
                Date d = sdf.parse(message.getDatetime());

                String formattedDate = new SimpleDateFormat("dd.MM.yyyy - HH:mm", Locale.GERMANY).format(d);

                timeText.setText(formattedDate);
            } catch (ParseException ex) {
                Log.d("Dateparse", ex.getLocalizedMessage());
            }
        }
    }

    class SentLocationHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {
        TextView timeLocation;
        MapView mapView;
        GoogleMap map;
        LatLng location;

        SentLocationHolder(View itemView) {
            super(itemView);

            mapView = itemView.findViewById(R.id.location_map);
            timeLocation = itemView.findViewById(R.id.location_message_time);

            if (mapView != null) {
                // Initialise the MapView
                mapView.onCreate(null);
                // Set the map ready callback to receive the GoogleMap object
                mapView.getMapAsync(this);
            }
        }

        void bind(Message message) {
            String[] temp = message.getData().split(",");

            location = new LatLng(Double.parseDouble(temp[0]), Double.parseDouble(temp[1]));
            setMapLocation();


            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.GERMANY);
            try {
                Date d = sdf.parse(message.getDatetime());

                // Format the stored timestamp into a readable String using method.
                String formattedDate = new SimpleDateFormat("dd.MM.yyyy - HH:mm", Locale.GERMANY).format(d);

                timeLocation.setText(formattedDate);
            } catch (ParseException ex) {
                Log.d("Dateparse", ex.getLocalizedMessage());
            }
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            MapsInitializer.initialize(mContext);
            map = googleMap;
            setMapLocation();
        }

        private void setMapLocation() {
            if (map == null) return;
            if (location == null) return;

            // Add a marker for this item and set the camera
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 17f));
            map.addMarker(new MarkerOptions().position(location));

            // Set the map type back to normal.
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }
    }

    class ReceivedLocationHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {
        TextView timeLocation;
        MapView mapView;
        GoogleMap map;
        LatLng location;
        ImageView profileImage;

        ReceivedLocationHolder(View itemView) {
            super(itemView);

            mapView = itemView.findViewById(R.id.location_map);
            timeLocation = itemView.findViewById(R.id.location_message_time);
            profileImage = itemView.findViewById(R.id.image_message_profile);

            if (mapView != null) {
                // Initialise the MapView
                mapView.onCreate(null);
                // Set the map ready callback to receive the GoogleMap object
                mapView.getMapAsync(this);
            }
        }

        void bind(Message message) {
            String[] temp = message.getData().split(",");

            location = new LatLng(Double.parseDouble(temp[0]), Double.parseDouble(temp[1]));
            setMapLocation();


            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.GERMANY);
            try {
                Date d = sdf.parse(message.getDatetime());

                // Format the stored timestamp into a readable String using method.
                String formattedDate = new SimpleDateFormat("dd.MM.yyyy - HH:mm", Locale.GERMANY).format(d);

                timeLocation.setText(formattedDate);
            } catch (ParseException ex) {
                Log.d("Dateparse", ex.getLocalizedMessage());
            }
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            MapsInitializer.initialize(mContext);
            map = googleMap;
            setMapLocation();
        }

        private void setMapLocation() {
            if (map == null) return;
            if (location == null) return;

            // Add a marker for this item and set the camera
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 17f));
            map.addMarker(new MarkerOptions().position(location));

            // Set the map type back to normal.
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }
    }

    class SentImageHolder extends RecyclerView.ViewHolder {
        TextView timeText;
        ImageView imageView;

        SentImageHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image_view);
            timeText = itemView.findViewById(R.id.message_time);
        }

        void bind(Message message) {
            String response = message.getData();
            String[] byteValues = response.substring(1, response.length() - 1).split(",");
            byte[] byteArray = new byte[byteValues.length];

            for (int i = 0, len = byteArray.length; i < len; i++) {
                byteArray[i] = Byte.parseByte(byteValues[i].trim());
            }

            final Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

            imageView.post(new Runnable() {
                @Override
                public void run() {
                    imageView.setImageBitmap(Bitmap.createScaledBitmap(bmp, imageView.getWidth(), imageView.getHeight(), false));

                }
            });

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.GERMANY);
            try {
                Date d = sdf.parse(message.getDatetime());

                // Format the stored timestamp into a readable String using method.
                String formattedDate = new SimpleDateFormat("dd.MM.yyyy - HH:mm", Locale.GERMANY).format(d);

                timeText.setText(formattedDate);
            } catch (ParseException ex) {
                Log.d("Dateparse", ex.getLocalizedMessage());
            }
        }
    }

    class ReceivedImageHolder extends RecyclerView.ViewHolder {
        TextView timeText;
        ImageView imageView;

        ReceivedImageHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image_view);
            timeText = itemView.findViewById(R.id.message_time);
        }

        void bind(Message message) {
            String response = message.getData();
            String[] byteValues = response.substring(1, response.length() - 1).split(",");
            byte[] byteArray = new byte[byteValues.length];

            for (int i = 0, len = byteArray.length; i < len; i++) {
                byteArray[i] = Byte.parseByte(byteValues[i].trim());
            }

            final Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

            imageView.post(new Runnable() {
                @Override
                public void run() {
                    imageView.setImageBitmap(Bitmap.createScaledBitmap(bmp, imageView.getWidth(), imageView.getHeight(), false));

                }
            });

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.GERMANY);
            try {
                Date d = sdf.parse(message.getDatetime());

                // Format the stored timestamp into a readable String using method.
                String formattedDate = new SimpleDateFormat("dd.MM.yyyy - HH:mm", Locale.GERMANY).format(d);

                timeText.setText(formattedDate);
            } catch (ParseException ex) {
                Log.d("Dateparse", ex.getLocalizedMessage());
            }
        }
    }

    // endregion

    ChatRecyclerAdapter(Context context) {
        mContext = context;
    }

    // Determines the appropriate ViewType according to the sender of the message.
    @Override
    public int getItemViewType(int position) {
        Message message = mMessageList.get(position);
        switch (message.getMimetype()) {
            case "text/plain":
                if (message.getSender().equals(getUser())) {
                    // If the current user is the sender of the message
                    return VIEW_TYPE_MESSAGE_SENT;
                } else {
                    // If some other user sent the message
                    return VIEW_TYPE_MESSAGE_RECEIVED;
                }
            case "webView/plain":
                if (message.getSender().equals(getUser())) {
                    return VIEW_TYPE_LOCATION_SENT;
                } else {
                    return VIEW_TYPE_LOCATION_RECEIVED;
                }
            case "image/bmp":
                if (message.getSender().equals(getUser())) {
                    return VIEW_TYPE_IMAGE_SENT;
                } else {
                    return VIEW_TYPE_IMAGE_RECEIVED;
                }
            default:
                return 0;
        }
    }

    // Inflates the appropriate layout according to the ViewType.
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEW_TYPE_MESSAGE_SENT:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.message_sent, parent, false);
                return new SentMessageHolder(view);
            case VIEW_TYPE_MESSAGE_RECEIVED:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.message_received, parent, false);
                return new ReceivedMessageHolder(view);
            case VIEW_TYPE_LOCATION_SENT:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.location_sent, parent, false);
                return new SentLocationHolder(view);
            case VIEW_TYPE_LOCATION_RECEIVED:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.location_received, parent, false);
                return new ReceivedLocationHolder(view);
            case VIEW_TYPE_IMAGE_SENT:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.image_sent, parent, false);
                return new SentImageHolder(view);
            case VIEW_TYPE_IMAGE_RECEIVED:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.image_received, parent, false);
                return new ReceivedImageHolder(view);
        }
        return null;
    }

    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Message message = mMessageList.get(position);

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_LOCATION_SENT:
                ((SentLocationHolder) holder).bind(message);
                break;
            case VIEW_TYPE_LOCATION_RECEIVED:
                ((ReceivedLocationHolder) holder).bind(message);
                break;
            case VIEW_TYPE_IMAGE_SENT:
                ((SentImageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_IMAGE_RECEIVED:
                ((ReceivedImageHolder) holder).bind(message);
                break;
        }
    }

    private String getUser() {
        final SharedPreferences sp = new SecurePreferences(mContext, mContext.getSharedPreferences("loginPalaver", Context.MODE_PRIVATE));
        return sp.getString("Username", "");
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    void setDataset(ArrayList<Message> data) {
        mMessageList = data;
    }
}
