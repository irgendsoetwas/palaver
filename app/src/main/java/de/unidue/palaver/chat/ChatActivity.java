package de.unidue.palaver.chat;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import de.unidue.palaver.MainActivity;
import de.unidue.palaver.R;
import de.unidue.palaver.save.SaveHandler;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class ChatActivity extends AppCompatActivity {

    private String friend;
    private ChatRecyclerAdapter mAdapter;
    private RecyclerView recyclerView;
    private FusedLocationProviderClient flpClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        // Get friend via extra intent
        friend = getIntent().getStringExtra("recipient");
        getSupportActionBar().setTitle(friend);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // 1. get RecyclerView
        recyclerView = findViewById(R.id.reyclerview_message_list);
        final MessageHandler messageHandler = new MessageHandler(this, getApplicationContext());

        // 2. set layoutManger
        LinearLayoutManager llm = new LinearLayoutManager(ChatActivity.this);
        llm.setStackFromEnd(true);
        recyclerView.setLayoutManager(llm);

        // 3. create an adapter
        mAdapter = new ChatRecyclerAdapter(this);

        // 4. set adapter
        recyclerView.setAdapter(mAdapter);

        if (MainActivity.online) {
            messageHandler.getMessage(friend);
        } else {
            SaveHandler db = new SaveHandler(getApplicationContext());
            ArrayList<Message> msgs = db.getAllMessages(friend);
            if (!msgs.isEmpty()) {
                setMessages(msgs);
            }
        }

        // 5. set item animator to DefaultAnimator
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter.notifyDataSetChanged();


        // Send messages
        final EditText mts = findViewById(R.id.edittext_chatbox);
        if (MainActivity.online) {
            findViewById(R.id.button_chatbox_send).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!TextUtils.isEmpty(mts.getText())) {
                        messageHandler.sendMessage(friend, mts.getText().toString(), "text/plain");
                        mts.getText().clear();
                    }
                }
            });
        }

        findViewById(R.id.edittext_chatbox).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                scrollToBottom();
            }
        });

        recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (bottom < oldBottom) {
                    recyclerView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            scrollToBottom();
                        }
                    }, 100);
                }
            }
        });
    }

    // Callback Method from MessageHandler
    public void setMessages(ArrayList<Message> messages) {

        if (!messages.isEmpty()) {
            mAdapter.setDataset(messages);

            recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.notifyDataSetChanged();
                }
            });

        } else {
            Log.d("temp", "ist leer");
        }

    }

    public void saveMessages(ArrayList<Message> messages) {

    }

    public void reloadMessages() {
        final MessageHandler mh = new MessageHandler(this, getApplicationContext());
        mh.getMessage(friend);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.action_location:
                flpClient = LocationServices.getFusedLocationProviderClient(this);
                Log.d("menu", "Location clicked");
                if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermission();
                }
                if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    flpClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            Log.d("location", "callback");
                            if (location != null) {
                                Log.d("location", "yay" + location.getLatitude() + " --- " + location.getLongitude());
                                new MessageHandler(ChatActivity.this, getApplicationContext()).sendMessage(friend, location.getLatitude() + "," + location.getLongitude(), "webView/plain");
                            }
                        }
                    });
                }
                return true;
            case R.id.action_files:
                Intent intent = new Intent().setType("image/*").setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Bild auswählen"), 123);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chat_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // requestCode 123 = sendFile
        if (requestCode == 123 && resultCode == RESULT_OK) {
            Uri selectedfile = data.getData(); //The uri with the location of the file
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedfile);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                new MessageHandler(ChatActivity.this, getApplicationContext()).sendMessage(friend, Arrays.toString(byteArray), "image/bmp");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void scrollToBottom() {
        int msgCount = recyclerView.getAdapter().getItemCount();
        if (msgCount > 1) {
            recyclerView.smoothScrollToPosition(msgCount - 1);
        }

        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, 1);
    }

    private byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }
}
