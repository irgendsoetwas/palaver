package de.unidue.palaver.network;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import de.unidue.palaver.MainActivity;
import de.unidue.palaver.data.SecurePreferences;

public class PushTokenHandler implements CustomCallback {

    private MainActivity mainActivity;
    private Context context;

    public PushTokenHandler(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.context = mainActivity.getApplicationContext();
    }

    public void transmitToken(String token) {
        try {
            final SharedPreferences sp = new SecurePreferences(context, context.getSharedPreferences("loginPalaver", Context.MODE_PRIVATE));
            String username = sp.getString("Username", "");
            String password = sp.getString("Password", "");

            JSONObject jsonParam = new JSONObject();
            jsonParam.put("Username", username);
            jsonParam.put("Password", password);
            jsonParam.put("PushToken", token);

            HttpAsyncTask task = new HttpAsyncTask(jsonParam, RequestType.TOKEN, this);
            task.execute("http://palaver.se.paluno.uni-due.de/api/user/pushtoken");
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void completionHandler(Boolean success, RequestType type, ServerResponse responseObject) {
        if (type == RequestType.TOKEN) {
            if (success) {
                if (responseObject.isSuccess()) {
                    Log.d("PushTokenHandler", "success! " + responseObject.getInfo());
                }
                else {
                    Log.d("PushTokenHandler", responseObject.getInfo()); // entweder Benutzer existiert nicht oder Passwort falsch
                }
            }
            else {
                Log.d("login", "failed getting data");
                // TODO handle fatal error when getting data
            }
        }
    }
}
