package de.unidue.palaver.network;

public class ServerResponse {
    private int msgType;
    private String info;
    private String data;

    public ServerResponse (int msgType, String info, String data) {
        this.msgType = msgType;
        this.info = info;
        this.data = data;
    }

    public int getMsgType() {
        return msgType;
    }

    public Boolean isSuccess() {
        return msgType != 0;
    }

    public String getInfo() {
        return info;
    }

    public String getData() {
        return data;
    }
}
