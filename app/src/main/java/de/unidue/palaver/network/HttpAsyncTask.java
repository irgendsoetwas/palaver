package de.unidue.palaver.network;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class HttpAsyncTask extends AsyncTask<String, Void, Void> {

    private JSONObject postData;
    private RequestType type;
    private CustomCallback callback;
    private HttpURLConnection urlConnection;

    public HttpAsyncTask(JSONObject postData, RequestType type, CustomCallback callback) {
        if (postData != null) {
            this.postData = postData;
        }
        this.type = type;
        this.callback = callback;
    }

    @Override
    protected Void doInBackground(String... params) {
        try {
            // This is getting the url from the string we passed in
            URL url = new URL(params[0]);

            // Create the urlConnection
            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);

            urlConnection.setRequestProperty("Content-Type", "application/json");

            urlConnection.setRequestMethod("POST");

            // Send the post body
            if (this.postData != null) {
                OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                writer.write(postData.toString());
                writer.flush();
            }

            int statusCode = urlConnection.getResponseCode();

            if (statusCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());

                Scanner s = new Scanner(inputStream).useDelimiter("\\A");
                String response = s.hasNext() ? s.next() : "";

                // Log.d("httpRequest", response);

                JSONObject responseJson = new JSONObject(response);
                ServerResponse serverResponse = new ServerResponse(
                        responseJson.getInt("MsgType"),
                        responseJson.getString("Info"),
                        responseJson.getString("Data"));

                callback.completionHandler(true, type, serverResponse);

            } else {
                // Status code is not 200
                // Do something to handle the error
                callback.completionHandler(false, type, null);
            }

        } catch (Exception e) {
            Log.d("HttpAsyncTask", "Error in AsyncTask");
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }
        return null;
    }

}
