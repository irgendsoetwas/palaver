package de.unidue.palaver.network;

import android.app.RemoteInput;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import de.unidue.palaver.data.SecurePreferences;

public class ReplyReceiver extends BroadcastReceiver implements CustomCallback {

    public ReplyReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);

        if (remoteInput != null) {
            int conversationId = intent.getIntExtra(MyFMS.CONVERSATION_ID, -1);
            String sender = intent.getStringExtra(MyFMS.SENDER_ID);

            String input = remoteInput.getCharSequence(MyFMS.KEY_NOTIFICATION_REPLY).toString();
            Log.d("notification", "Sender: " + sender + " reply: " + input);

            try {
                final SharedPreferences sp = new SecurePreferences(context, context.getSharedPreferences("loginPalaver", Context.MODE_PRIVATE));
                String username = sp.getString("Username", "");
                String password = sp.getString("Password", "");
                // this.mimetype = mimetype;

                JSONObject json = new JSONObject();
                json.put("Username", username);
                json.put("Password", password);
                json.put("Recipient", sender);
                json.put("Mimetype", "text/plain");
                json.put("Data", input);

                HttpAsyncTask task = new HttpAsyncTask(json, RequestType.SEND_MESSAGE, this);
                task.execute("http://palaver.se.paluno.uni-due.de/api/message/send");
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            NotificationManagerCompat notificationManager =
                    NotificationManagerCompat.from(context);
            //notificationManager.cancel(conversationId);
            notificationManager.cancelAll();
        }
    }

    @Override
    public void completionHandler(Boolean success, RequestType type, ServerResponse responseObject) {

    }
}
