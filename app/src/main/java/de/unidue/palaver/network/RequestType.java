package de.unidue.palaver.network;

public enum RequestType {
    LOGIN,
    REGISTER,
    TOKEN,
    PASSWORD_CHANGE,
    FRIENDLIST,
    CHECK_FRIEND,
    GET_MESSAGE,
    SEND_MESSAGE
}
