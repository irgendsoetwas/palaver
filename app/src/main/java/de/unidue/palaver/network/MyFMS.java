package de.unidue.palaver.network;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.support.v4.app.RemoteInput;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import de.unidue.palaver.MainActivity;
import de.unidue.palaver.R;
import de.unidue.palaver.chat.ChatActivity;
import de.unidue.palaver.data.SecurePreferences;

public class MyFMS extends FirebaseMessagingService {

    private NotificationManager notificationManager;
    public static final String CHANNEL_ID = "palaverChannel";
    public static final String KEY_NOTIFICATION_REPLY = "KEY_NOTIFICATION_REPLY";
    public static final String CONVERSATION_ID = "CONVERSATION_ID";
    public static final String SENDER_ID = "SENDER_ID";

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Log.d("Firebase", "Refreshed token: " + token);

        PushTokenHandler nh = new PushTokenHandler((MainActivity) getApplicationContext());
        nh.transmitToken(token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        //Setting up Notification channels for android O and above
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setupChannels();
        }
        int notificationId = getNextNotifId(getApplicationContext());
        String sender = remoteMessage.getData().get("sender");

        // Default Notification sound
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        // TODO change activity to chat view with sender
        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("recipient", sender);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        // Define PendingIntent for Reply action
        PendingIntent replyPendingIntent;
        if (Build.VERSION.SDK_INT < 24) {
            replyPendingIntent = pendingIntent;
        } else {
            Intent replyIntent = new Intent(this, ReplyReceiver.class)
                    .putExtra(CONVERSATION_ID, notificationId)
                    .putExtra(SENDER_ID, sender);
            replyPendingIntent = PendingIntent.getBroadcast(this, 0, replyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        // Create RemoteInput and attach it to Notification Action
        RemoteInput remoteInput = new RemoteInput.Builder(KEY_NOTIFICATION_REPLY).setLabel("Antworten").build();
        NotificationCompat.Action replyAction = new NotificationCompat.Action.Builder(
                android.R.drawable.ic_menu_save, "Antworten", replyPendingIntent)
                .addRemoteInput(remoteInput).build();

        // Build Notification
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(sender)
                .setContentText(remoteMessage.getData().get("preview"))
                .setAutoCancel(true)  // dismisses the notification on click
                .setSound(defaultSoundUri)
                .setLights(0xff00ff00, 300, 100)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .addAction(replyAction);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(notificationId, notificationBuilder.build());

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels() {
        CharSequence adminChannelName = getString(R.string.notifications_channel_name);
        String adminChannelDescription = getString(R.string.notifications_channel_description);

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_HIGH);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }

    private int getNextNotifId(Context context) {
        String LAST_NOTIF_ID = "LAST_NOTIF_ID";
        SharedPreferences sp = new SecurePreferences(context, context.getSharedPreferences("LAST_NOTIF_ID", Context.MODE_PRIVATE));
        int id = sp.getInt(LAST_NOTIF_ID, 0) + 1;
        if (id == Integer.MAX_VALUE) {
            id = 0;
        }
        sp.edit().putInt(LAST_NOTIF_ID, id).apply();
        return id;
    }
}
