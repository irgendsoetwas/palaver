package de.unidue.palaver.network;

public interface CustomCallback {
    // Callback from HttpAsyncTask to interact with other components
    // The object in the completionHandler will be whatever it is that you need to send to your controllers

    void completionHandler(Boolean success, RequestType type, ServerResponse responseObject);
}
